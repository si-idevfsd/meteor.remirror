import OrderedMap from "orderedmap"
import { NodeSpec, MarkSpec } from "prosemirror-model"

function makeOrderedMap<T> (content: Array<string|T>) {
  // Sigh. We can be called from unit tests that want to catch changes
  // in undocumented internals, in particular as far as
  // /imports/api/schema-spec-ejson is concerned. Therefore, we shall
  // go through the motions and construct our OrderedMap's using only
  // the public API thereof. Unfortunately, it looks like this cannot
  // be done in less than O(N^2) complexity! (The undocumented
  // constructor with the exact same calling convention as
  // makeOrderedMap() is O(1), natch)
    let m = OrderedMap.from<T>()
    while(content.length) {
        const [k, v] = content.splice(0, 2) as [ string, T ]
        m = m.addToEnd(k, v)
    }
    return m
}

/**
 * The schema of the {@link WysiwygEditor} from '@remirror/editor-wysiwyg'
 */
export const wysiwygSchemaSpec = {
    nodes: makeOrderedMap<NodeSpec>([
        "doc", {"content":"block+"},
        "text", {"group":"inline"},
        "paragraph", {"content":"inline*","group":"block","attrs":{"align":{"default":null},"id":{"default":null},"indent":{"default":0},"lineSpacing":{"default":null}},"draggable":false,"parseDOM":[{"tag":"p"}]},
        "blockquote", {"attrs":{},"content":"block*","group":"block","defining":true,"draggable":false,"parseDOM":[{"tag":"blockquote"}]},
        "heading", {"attrs":{"level":{"default":1}},"content":"inline*","group":"block","defining":true,"draggable":false,"parseDOM":[{"tag":"h1","attrs":{"level":1}},{"tag":"h2","attrs":{"level":2}},{"tag":"h3","attrs":{"level":3}},{"tag":"h4","attrs":{"level":4}},{"tag":"h5","attrs":{"level":5}},{"tag":"h6","attrs":{"level":6}}]},
        "horizontalRule", {"attrs":{},"group":"block","parseDOM":[{"tag":"hr"}]}
        ,"image", {"inline":true,"attrs":{"align":{"default":null},"alt":{"default":""},"crop":{"default":null},"height":{"default":null},"width":{"default":null},"rotate":{"default":null},"src":{"default":null},"title":{"default":""}},"group":"inline","draggable":true,"parseDOM":[{"tag":"img[src]"}]},
        "listItem", {"attrs":{},"content":"paragraph block*","defining":true,"draggable":false,"parseDOM":[{"tag":"li"}]},
        "bulletList", {"attrs":{},"content":"listItem+","group":"block","parseDOM":[{"tag":"ul"}]},
        "orderedList", {"attrs":{"order":{"default":1}},"content":"listItem+","group":"block","parseDOM":[{"tag":"ol"}]},
        "hardBreak", {"attrs":{},"inline":true,"group":"inline","selectable":false,"parseDOM":[{"tag":"br"}]},
        "codeBlock", {"attrs":{"language":{}},"content":"text*","marks":"","group":"block","code":true,"defining":true,"isolating":true,"draggable":false,"parseDOM":[{"tag":"pre","preserveWhitespace":"full"}]}
    ]),
    marks: makeOrderedMap<MarkSpec>([
        "link", {"group":"link","attrs":{"href":{"default":null}},"inclusive":false,"parseDOM":[{"tag":"a[href]"}]},
        "bold", {"group":"fontStyle","parseDOM":[{"tag":"strong"},{"tag":"b"},{"style":"font-weight"}]},

        "underline", {"group":"fontStyle","parseDOM":[{"tag":"u"},{"style":"text-decoration"}]},
        "italic", {"group":"fontStyle","parseDOM":[{"tag":"i"},{"tag":"em"},{"style":"font-style=italic"}]}
,
        "strike", {"group":"fontStyle","parseDOM":[{"tag":"s"},{"tag":"del"},{"tag":"strike"},{"style":"text-decoration"}]},
        "code", {"group":"code","parseDOM":[{"tag":"code"}]}
    ]),
}
