import { SerializableNode } from '../../imports/api/documents'

export const laCigaleJSON : SerializableNode = {
    type:"doc",
    content:[
        {
            type: "paragraph",
            attrs: {align:null, id:null, indent:0, lineSpacing:null},
            content:[
                {type:"text", text: "La "},
                {type:"text", marks: [{type:"italic"}], text: "cigale"},
                {type:"text", text: ", ayant chanté tout l'été..."}
            ]
        }
    ]
}
