import assert from "assert"
import OrderedMap from "orderedmap"
import { EJSON } from "meteor/ejson"
import "../imports/api/schema-spec-ejson"
import { wysiwygSchemaSpec } from "./fixtures/schema"

describe("SchemaSpec serialization", function() {
    it("uses an EJSON tag to serialize OrderedMap's", () => {
        const json = EJSON.stringify(wysiwygSchemaSpec)
        assert(json.match(/"[$]type":"OrderedMap"/))   // No assert.match in the browser
    })
    it("round-trips", () => {
        const wysiwygSchemaSpecToo = EJSON.parse(
            EJSON.stringify(wysiwygSchemaSpec)) as typeof wysiwygSchemaSpec
        assert(wysiwygSchemaSpecToo.marks instanceof OrderedMap)
        assert.deepEqual(wysiwygSchemaSpec.marks.get("underline"),
                         wysiwygSchemaSpecToo.marks.get("underline"))
    })
})
