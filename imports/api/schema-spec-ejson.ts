/**
 * serialize / deserialize ProseMirror SchemaSpec's
 *
 * All the types making up a SchemaSpec are primitive (“unblessed” if
 * you will, in Perl parlance - Functionless interfaces, in TypeScript
 * parlance) - except for OrderedMap's. Here, we therefore tell Meteor
 * how to serialize the latter.
 */
import { EJSON, EJSONableCustomType } from 'meteor/ejson'
import OrderedMap_ from 'orderedmap'

////////////////////////// Types and cheats //////////////////////////

const OrderedMap = OrderedMap_ as typeof OrderedMap_ & {
    // Use the undocumented constructor for deserialization, and trust
    // the unit tests
    new<T>(content?: Array<string|T>): OrderedMap_<T>
}

const OrderedMapPrototype = OrderedMap.prototype as
typeof OrderedMap.prototype & EJSONableCustomType & {
    // Use the undocumented sole data field for serialization
    content: Array<string|any>
}

/////////////////////////// Implementation ///////////////////////////

OrderedMapPrototype.typeName = () => 'OrderedMap'
OrderedMapPrototype.toJSONValue = function() {
    return { content: this.content }
}

EJSON.addType(OrderedMapPrototype.typeName(), function (repr: any) : any {
    return new OrderedMap(repr.content)
})
