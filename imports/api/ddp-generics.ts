/**
 * Type-checked DDP API endpoints
 *
 * Using this module helps prevent type mismatches between client and
 * server, and other common mistakes when designing medium-to-complex
 * DDP APIs.
 */

import { Meteor, Subscription as MeteorSubscription } from 'meteor/meteor'
import { Mongo } from 'meteor/mongo'

/**
 * Like MeteorSubscription, except the collection name (the
 * `collection` string parameter in methods) is made available to the
 * server implementation, and the type of the collection items is
 * checked.
 */
interface Subscription<O> {
    added(id: string, fields: O): void
    changed(id: string, fields: O): void
    connection: Meteor.Connection
    error(error: Error): void
    onStop(func: Function): void
    ready(): void
    removed(id: string): void
    stop(): void
    userId: string
    collectionName: string
}

/**
 * Partially apply the `collection` string parameter to a MeteorSubscription API
 *
 * This frees publish function authors from having to pass the
 * collection name around, which is particularly handy for {@link
 * clientNamedPublication} in which that name cannot be predicted.
 */
function currySubscription<O>(
    collectionName: string, that : MeteorSubscription) : Subscription<O>
{
    return {
        added: (id : string, fields : O) => that.added(collectionName, id, fields),
        changed: (id : string, fields : O) => that.changed(collectionName, id, fields),
        connection: that.connection,
        error: that.error.bind(that),
        onStop: that.onStop.bind(that),
        ready: that.ready.bind(that),
        removed: (id : string) => that.removed(collectionName, id),
        stop: that.stop.bind(that),
        userId: that.userId,
        collectionName
    }
}

let clientNamedPublicationSequence = 0

/**
 * A DDP publication whose on-the-wire publication name is picked by the client
 */
export class ClientNamedPublication<I extends any[], O> {
    constructor(private ddpName: string) {
        if (Meteor.isClient) {
            this.collectionName = ddpName + "_" + clientNamedPublicationSequence++
            this.collection = new Mongo.Collection(this.collectionName)
        }
    }

    /**
     * @locus client
     */
    collection: Mongo.Collection<O> =
        null as any // On server only - See constructor

    /**
     * @locus client
     */
    collectionName : string =
        null as any // On server only - See constructor

    /**
     * Subscribe to the collection.
     *
     * @returns An object which has both the API of a Promise (for the "ready" event)
     *          and all the API returned by Meteor's {@link Meteor#subscribe} method.
     *
     * @locus client
     */
    subscribe(...args: I) : Meteor.SubscriptionHandle & Promise<void> {
        const subscribeArgs : any[] = args
        let subscriptionHandle: Meteor.SubscriptionHandle
            = null as any    // SubscriptionHandle is guaranteed to be set before
                             // we return, but TypeScript doesn't know that
        const ready : Promise<void> = new Promise<void>((resolve, reject) => {
            subscribeArgs.push({
                onReady() { resolve() },
                onStop(e?: Meteor.Error) { reject(e) }
            })
            subscriptionHandle = Meteor.subscribe(this.ddpName, this.collectionName,
                                                  ...subscribeArgs)
        })

        const readyDelegate: Promise<void> = {
            // Must be made up of own properties only, lest Object.assign() won't
            then: (...args: any[]) => ready.then(...args),
            catch: (...args: any[]) => ready.catch(...args),
            finally: (...args: any[]) => ready.finally(...args),
            [Symbol.toStringTag]: ready[Symbol.toStringTag]
        }

        return Object.assign(subscriptionHandle, readyDelegate)
    }

    /**
     * @locus server
     */
    publish(f: (this: Subscription<O>, ...args: I) => void): void {
        // Curry away collectionName; feed the other args to f
        Meteor.publish(this.ddpName, function(collectionName: string, ...args) {
            f.call(currySubscription<O>(collectionName, this), ...args as I)
        })
    }

    /**
     * Publish out of an async function.
     *
     * When the function returns, the collection will be marked ready
     * automatically. If the function throws, the failure will be
     * propagated to the client.
     *
     * @locus server
     */
    publishAsync(f: (this: Subscription<O>, ...args: I) => Promise<void>): void {
        Meteor.publish(this.ddpName, function(collectionName: string, ...args) {
            f.call(currySubscription<O>(collectionName, this), ...args as I)
            .then(() => this.ready())
            .catch((err) => this.error(err))
        })
    }
}

/**
 * A Meteor DDP method
 */
export class Method<I extends any[], O> {
    constructor(private ddpName: string) {}

    /**
     * @locus client
     */
    async call(...args: I) : Promise<O> {
        return await Meteor.callAsync(this.ddpName, ...args)
    }

    /**
     * Implement this method with an asynchronous function, while still
     * not allowing several calls from the same client to run in parallel
     * (unless `f` explicitly calls `this.unblock()`)
     *
     * @locus server
     */
    implementAsync(f: (this: Meteor.MethodThisType, ...args: I) => Promise<O>) {
        return Meteor.methods({[this.ddpName]: async function(...args) {
            return await f.apply(this,  // ⚠ Meteor's `this`, not the same as
                                        // the one on the previous line
                args as I)
        }})
    }
}
