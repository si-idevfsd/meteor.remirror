/**
 * ProseMirror document CRUD API over DDP
 *
 * The ProseMirror-over-DDP protocol is designed to fulfill the
 * requirements of ProseMirror's collaborative editing feature, as
 * described here: https://prosemirror.net/docs/guide/#collab
 */

import './schema-spec-ejson'

import { SchemaSpec } from 'prosemirror-model'
import { ClientNamedPublication, Method } from './ddp-generics'

// Serializable types, aliased for clarity
export type ProseMirrorVersionID = number
export type SerializableStep = Object

/**
 * What {@link node#toJSON} returns (or a close approximation thereof)
 *
 * @remarks
 * This is basically a trimmed version of {@link @remirror/core#ObjectNode},
 * but we want /imports/api/documents.ts to be React-agnostic so we have
 * to refrain from just importing/reexporting that.
 */
export interface SerializableNode {
    type: string
    marks?: Array<SerializableMark | string>
    text?: string
    content?: SerializableNode[]
    attrs?: Record<string, any>
}
interface SerializableMark {
    type: string;
    attrs?: Record<string, string | null>;
}

export type DocumentVersion = InitialDocumentVersion | SubsequentDocumentVersion

/**
 * The type of the very first DocumentVersion in
 * {@link ProseMirrorDDPAPI}'s `getDocument.collection`.
 */
export interface InitialDocumentVersion {
    /**
     * A unique, ascending sequence number for this document.
     *
     * Items in the collection created by {@link
     * DDPSubscribe__GetDocument} are keyed by (that is, have the same
     * `._id` as) their `.version`.
     */
    version: ProseMirrorVersionID
    /**
     * The ProseMirror schema specification for this document.
     *
     * Reserved for the very first DocumentVersion returned by {@link
     * DDPSubscribe__GetDocument}.
     */
    schemaSpec: SchemaSpec
    /**
     * The initial content of this document, in ProseMirror format.
     */
    content: SerializableNode
}

export interface SubsequentDocumentVersion {
    /**
     * A unique, ascending sequence number for this document.
     *
     * Items in the collection created by {@link
     * DDPSubscribe__GetDocument} are keyed by (that is, have the same
     * `._id` as) their `.version`.
     */
    version: ProseMirrorVersionID

    /**
     * The server-specified client ID of the author of this step. Making
     * sense of the client ID through some kind of user lookup mechanism is
     * beside the scope of this API.
     */
    clientID: string

    /**
     * The ProseMirror step to apply on top of the previous version.
     *
     * Pursuant to https://prosemirror.net/docs/guide/#collab, the
     * receiving client is to apply this step to the document with
     * version `(version - 1)`.
     */
     step: SerializableStep

    /**
     * The new document content, after the `.step` has been applied.
     *
     * If received as part of a SubsequentDocumentVersion, this
     * indicates that the server wants to perform a “resync check”.
     * Clients apply the `.step` as usual, and in case they find a
     * discrepancy (i.e. their own content differs from this
     * DocumentVersion's `.content` after applying this
     * DocumentVersion's `.step`) then they should let the server
     * know about the problem by unsubscribing/resubscribing.
     */
    content?: SerializableNode
}

export class ProseMirrorDDPAPI {
    /**
     * DDP subscription to register interest in the contents of the
     * document, and the subsequent changes to it.
     *
     * The client shall call `getDocument.subscribe(docID)` where
     * `docID` is a document ID whose meaning and namespace is
     * unspecified in this API, and left at the sole discretion of the
     * server. Afterwards, `getDocument.collection` will be populated
     * with **versions** of the document (in the ProseMirror sense),
     * indexed by the ProseMirror version number. The first version
     * contains a serialization of the ProseMirror schema and the
     * original state ( (see {@link InitialDocumentVersion}), while
     * the subsequent ones contain deltas, serialized as per
     * ProseMirror's collaboration protocol (see {@link
     * SubsequentDocumentVersion}).
     *
     * There is no expectation that the elements in
     * `getDocument.collection` be “real” (i.e. persisted in a MongoDB
     * on the server), and in fact the DDP-level name of this
     * collection will differ between all instances of
     * `ProseMirrorDDPAPI`. An efficient server-side implementation
     * will typically persist (and update) the
     * `InitialDocumentVersion`, and circulate only the ProseMirror
     * steps among DDP clients that are currently subscribed to the
     * same document ID, generally declining to persist said steps
     * (or even the older document versions) except perhaps for
     * archival, auditing and/or forensics purposes.
     *
     * Once this subscription is unsubscribed (which happens by
     * calling the `.unsubscribe()` method on the object returned by
     * `.getDocument.subscribe()`; or by closing the DDP connection),
     * the server stops streaming updates in the way explained above.
     * The client may “catch up” by resubscribing later.
     */
    getDocument: ClientNamedPublication<[string], DocumentVersion>

    /**
     * Submit one or more locally-authored changes to the DDP server.
     *
     * Clients must be prepared handle two successful outcomes of this
     * call, namely "ok" and "rebase" as per
     * https://prosemirror.net/docs/guide/#collab . In all other
     * outcomes (i.e., exceptions), the user must be notified, and
     * must be given the option to discard their lost edits.
     *
     * **No client should have more than one `updateDocument` method
     * call outstanding at any given time** — Attempting to
     * "send-ahead" with `newVersion` arithmetic can cause
     * lost-update, and is a recipe for disaster. Instead, clients
     * shall keep their users' updates in a queue (outside of DDP) to
     * send or rebase later.
     *
     * @param {string} documentID
     *                      The ID of the document to mutate
     *
     * @param {string} collectionID
     *                      The collection ID of the matching {@link
     *                      getDocument} call
     *
     * @param {ProseMirrorVersionID} version
     *                      The ProseMirror version this change
     *                      applies to, i.e. the latest (numeric max)
     *                      successfully synced version known to the
     *                      client; whether from a {@link
     *                      DocumentVersion} (successfully applied to
     *                      the local copy), or from the `.newVersion`
     *                      of a previous, successful (`.status ===
     *                      "ok"`) call to `updateDocument`.
     *
     * @param {Array<SerializableStep>} steps
     *                      The list of steps this update consists of,
     *                      in ProseMirror format.
     *
     * @returns { UpdateDocumentResponse } response
     * @returns { UpdateDocumentStatus }   response.status
     *                     Either "ok" to indicate the server applied the
     *                     changes, or "rebase" to let the client know
     *                     that it lost the race for first-to-update -
     *                     in which case, the DDP method call shall be
     *                     repeated after rebasing the changes
     * @returns { UpdateDocumentStatus }   response.newversion
     *                     The last version known to the server. If
     *                     `status` is "ok", the client shall consider
     *                     `newVersion` as its current version; the
     *                     server is representing that it has synced
     *                     up to and including the steps sent as
     *                     part of this method call, and will be
     *                     sending them promptly to other clients
     *                     (although it will obviously not resend them
     *                     back to this client.) If `status` is
     *                     "rebase", the server is representing that
     *                     is is not willing to accept this
     *                     `updateDocument` method call, nor any
     *                     other, unless and until the client has
     *                     received and replayed all the {@link
     *                     DocumentVersion}s up to this `newVersion`.
     */
    updateDocument: Method<UpdateDocumentArgs,
                           UpdateDocumentResponse>

    constructor(suffix? : string) {
        suffix = (suffix ? '.' + suffix : '')
        this.getDocument = new ClientNamedPublication('prosemirror.getDocument' + suffix)
        this.updateDocument = new Method<UpdateDocumentArgs, UpdateDocumentResponse>(
            'prosemirror.updateDocument' + suffix)
    }
}

type UpdateDocumentArgs = [string, string, ProseMirrorVersionID, Array<SerializableStep>]

export const enum UpdateDocumentStatus {
    OK = "ok",          // Update was applied; newVersion is ours
    REBASE = "rebase"   // Client must rebase; updates will be ignored
                        // until we sync up to and including newVersion
}

export interface UpdateDocumentResponse {
    status: UpdateDocumentStatus
    newVersion: ProseMirrorVersionID
}

export const enum ErrorCode {
    DocumentNotFound = "DocumentNotFound"
}
