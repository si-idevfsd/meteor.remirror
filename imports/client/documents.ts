/**
 * Client-side state machine for the ProseMirror-over-DDP API
 */

import debug_ from 'debug'

import { Tracker } from 'meteor/tracker'
import { ReactiveVar as ReactiveVar_ } from 'meteor/reactive-var'
import { ReactiveArray } from './lib/reactive-array'
import { ProseMirrorDDPAPI, ProseMirrorVersionID, InitialDocumentVersion, SubsequentDocumentVersion, SerializableStep, UpdateDocumentStatus, ErrorCode } from '../api/documents'
import { EventEmitter } from 'events'   // Comes for free with Webpack, according to https://www.npmjs.com/package/events

import "../api/schema-spec-ejson"

const debug = debug_('tiptap:ProseMirrorClient')

/**
 * Client handle to perform the ProseMirror synchronization process for one document.
 *
 * @locus client
 *
 * UNIMPLEMENTED / for later:
 * - Server-initiated resync
 */
export class ProseMirrorClient implements Pick<EventEmitter, "on" | "once"> {
    private lastVersion = new ReactiveVar<ProseMirrorVersionID | undefined>(undefined)
    private pendingStepCount = new ReactiveVar<number>(0)

    private updatePending: boolean = false
    private events: EventEmitter = new EventEmitter()
    public on = this.events.on.bind(this.events)
    public once = this.events.once.bind(this.events)

    private ourValidatedUpdates = new OurUpdates()

    /**
     * The client ID that should be used to initialize the ProseMirror API.
     */
    public readonly prosemirrorClientID = "local"

    private ready: () => boolean

    constructor (private api: ProseMirrorDDPAPI, private documentID: string) {
        const handle = api.getDocument.subscribe(documentID)
        handle.catch((error) => this.events.emit("error", error))
        this.events.on("stop", () => handle.stop())
        this.ready = () => handle.ready()
    }

    /**
     * Obtain the initial document contents and metadata; a reactive data source
     *
     * @returns {InitialDocumentVersion|undefined} document
     */
    getInitialDocument () : InitialDocumentVersion | undefined {
        if (! this.ready()) return
        const initial = this.api.getDocument.collection
            .find({content: {$exists: true}}, {sort: {version: 1 }}).fetch()
        if (initial.length == 0) {
            this.events.emit("error", new Meteor.Error(
                ErrorCode.DocumentNotFound,
                `Did not receive any data from server for document ${this.documentID}`))
            return
        }
        if (! this.lastVersion.getNonreactive()) {
            this.lastVersion.set(initial[0].version)
        }
        return initial[0] as InitialDocumentVersion
    }

    /**
     * Obtain server-validated document mutations; a reactive data source.
     *
     * The definition of “server-validated” includes our own updates,
     * which are not transmitted back over the DDP subscription, but
     * will be returned by this method nonetheless. (This is congruent
     * with how the ProseMirror API expects to be notified of the
     * server having accepted locally-issued updates.)
     *
     * This method won't return anything, unless and until
     * {@link getInitialDocument} has returned something.
     *
     * @returns updates     A contiguous sequence of server-validated
     *                      ProseMirror steps, as an object with the
     *                      following fields and method:
     * @returns { number? } updates.version
     *                      The first version at which the updates
     *                      should be applied; will be undefined if
     *                      `updates.steps` is the empty list
     * @returns {Array<SubsequentDocumentVersion>} updates.steps
     *                      A list of objects with fields `.steps` (a list of one
     *                      ProseMirror step in serializable form), `.version` and
     *                      `.clientID` (the client ID as a string set
     *                      by the server, or the same as {@link
     *                      prosemirrorClientID} for local updates).
     *                      Note that this includes steps for our
     *                      “own” (local) updates, which despite not
     *                      being sent back through the DDP
     *                      subscription, must be replayed against the
     *                      ProseMirror API in order to advance its
     *                      state.
     */
    getUpdates () : { steps: Array<SubsequentDocumentVersion>,
                      version?: number } {
        const noUpdates = { version: undefined, steps: [], applied: () => {} }

        const lastVersion = this.lastVersion.get()
        if (! lastVersion) { return noUpdates }

        const ourUpdates = this.ourValidatedUpdates.get(),
            ourUpdatesCount = ourUpdates.list.length
        if (ourUpdatesCount && ourUpdates.list[0].version === lastVersion + 1) {
            // Since ourUpdates are synthetic, proper sequencing is guaranteed.
            debug(`Reapplying our own ${ourUpdatesCount} update(s) against ProseMirror`)
            this.lastVersion.set(lastVersion + ourUpdatesCount)
            ourUpdates.consume()
            return {
                version:   lastVersion,
                steps:     ourUpdates.list,
            }
        }

        const theirUpdates = this.api.getDocument.collection
                .find({version: {$gt: lastVersion}}, {sort: {version: 1 }})
                .fetch() as Array<SubsequentDocumentVersion>

        const contiguousUpdates = []
        for(let i : number = 0; i < theirUpdates.length; i++) {
            if (theirUpdates[i].version === lastVersion + i + 1) {
                contiguousUpdates.push(theirUpdates[i])
            } else {
                break
            }
        }

        if (! contiguousUpdates.length) return noUpdates
        debug(`${contiguousUpdates.length} contiguous updates out of ${theirUpdates.length}`)

        this.lastVersion.set(lastVersion + contiguousUpdates.length)
        return {
            version:   lastVersion,
            steps:     contiguousUpdates,
        }
    }

    /**
     * Abandon this ProseMirrorClient and free all resources.
     *
     * All event listeners are canceled. Calling any method thereafter
     * results in undefined behavior.
     */
    stop () {
        this.events.emit("stop")
        this.events.removeAllListeners()
    }

    /**
     * Send a batch of locally-authored steps to the server.
     *
     * As per the way the ProseMirror API works, this method is
     * expected to be called after every editor transaction, repeating
     * the same steps until they are not needed anymore (in the sense
     * that {@link getUpdates} has returned them). Therefore, only the
     * first call to send() in a series made in rapid succession will
     * actually be transmitted over DDP; the others are ignored for as
     * long as the first DDP method call is pending, under the
     * assumption that the ProseMirror API will call send() again once
     * it has received (possibly a subset of) its own updates through
     * {@link getUpdates}. Likewise, calls to `send()` that are
     * already known to be out-of-sequence (e.g. because we already
     * received updates of a higher version through the DDP
     * subscription) will be silently discarded, again under the
     * assumption that ProseMirror will re-issue the `send()` after
     * applying the outstanding changes.
     */
    send ({version, steps}: {version: ProseMirrorVersionID, steps: Array<SerializableStep>}) {
        steps = Array.prototype.slice.call(steps)
        this.pendingStepCount.set(steps.length)

        const debugSend = (blah: string, ...params: any[]) => debug('send(): ' + blah, ...params)

        const lastVersion = this.lastVersion.getNonreactive()
        if (! lastVersion) {
            throw new Error("Cannot send before receiving initial version")
        }
        if (version !== lastVersion) {
            if (version < lastVersion) {
                debugSend(`punting already-obsolete ${steps.length} steps (${version} vs. ${lastVersion})`)
            } else {
                debugSend(`CRITICAL: ProseMirror is predicting the future?!  ${steps.length} steps (${version} vs. ${lastVersion})`)
            }
            return
        }

        debugSend(`got ${steps.length} steps starting at version ${version}...`)
        if (this.updatePending) {
            debugSend('... but another update is pending')
            return  // Trust DDP to retry if disconnected
        }

        debugSend(`... sending them`)
        this.updatePending = true
        this.api.updateDocument.call(this.documentID, this.api.getDocument.collectionName,
                                     lastVersion, steps)
            .then(({status, newVersion}) => {
                this.updatePending = false

                if (status === UpdateDocumentStatus.OK) {
                    debugSend("Update successful, new version at server: %s", newVersion)
                    this.pendingStepCount.set(this.pendingStepCount.getNonreactive() -
                        steps.length)

                    debugSend(`... replaying them to ourselves`)
                    for (let i = 0; i < steps.length; i++) {
                        const replayedVersion = lastVersion + i + 1
                        this.ourValidatedUpdates.push({
                            clientID: this.prosemirrorClientID,
                            version: replayedVersion,
                            step: steps[i]
                        })
                    }
                } else {
                    debugSend("must rebase after %d (currently: %d)", newVersion, this.lastVersion)
                    // Do nothing; ProseMirror will call send() again after
                    // it has applied the incoming, server-published steps
                }
            })
            .catch((err : Error) => {
                this.events.emit("error", err)
                this.stop()
            })
    }

    /**
     * The number of locally-authored steps not yet acknowledged by
     * the server; a reactive data source
     */
    getPendingStepCount () {
        return this.pendingStepCount.get()
    }
}

class ReactiveVar<T> extends ReactiveVar_<T> {
    getNonreactive() : T {
        return Tracker.nonreactive<T>(() => this.get())
    }
}

/**
 * A FIFO list of mock changes, that we replay to ourselves once the
 * updateDocument DDP method call succeeds.
 */
class OurUpdates {
    private updates = new ReactiveArray<SubsequentDocumentVersion>()

    get () {
        const current = Array.prototype.slice.call(this.updates.get())

        return {
            list: current,
            consume: () => { this.updates.splice(0, current.length) }
        }
    }

    push (...things: SubsequentDocumentVersion[]) {
        this.updates.push(...things)
    }
}
