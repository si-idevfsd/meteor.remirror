import { Tracker } from "meteor/tracker"
import { useEffect } from "react"

import debug_ from "debug"
const debug = debug_("tracker-effects")

/**
 * Run a `Tracker.autorun` function as a React effect.
 *
 * When the React effect terminates (i.e. because the calling React
 * component is being destroyed), stop the Tracker computation.
 *
 * @argument f    A function that will be passed to Tracker#autorun
 * @argument deps Optional dependency array that will be passed as
 *                the second argument to React#useEffect
 */
export function useTrackerAutorunEffect(
    f : (computation: Tracker.Computation) => void,
    deps? : React.DependencyList
) : void {
    useEffect(() => {
        function id(computation : Tracker.Computation) : Number {
            return (computation as any)._id
        }
        debug(`Starting computation for ${String(f)}...`)
        const computation = Tracker.autorun((computation) => {
            debug(`Running computation ${id(computation)}`)
            f(computation)
        })
        debug(`... started (id ${id(computation)})`)
        return () => {
            debug(`Stopped computation ${id(computation)}`)
            computation.stop()
        }
    }, deps)
}
