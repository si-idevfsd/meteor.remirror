// Same idea as
// https://github.com/ManuelDeLeon/ReactiveArray/blob/master/ReactiveArray.js
// with type checking and additional ES2015 Array accessors

import { Tracker } from 'meteor/tracker'

interface ReactiveArrayConstructor {
    <T>(...elements: Array<T>): ReactiveArrayInstance<T>
    new <T> (...elements: Array<T>): ReactiveArrayInstance<T>
}
export const ReactiveArray = makeReactiveArray as ReactiveArrayConstructor

interface ReactiveArrayInstance<T> {
    get() : Array<T>
    get(index : number) : T

    getNonreactive(): Array<T>
    // Unfortunately, we can't have writers that are also readers.
    // Since we cannot know whether the caller will use the return
    // value or discard it, we couldnt' accurately let Tracker know
    // about the read intent, which would cause bugs (failure to
    // re-run computations timely, or infinite loops, depending on
    // which way we err). Therefore, all writer accessors return void.
    push(...args: Array<T>): void
    splice(start: number, deleteCount?: number, ...newItems: Array<T>): void
}

function makeReactiveArray<T>(...elements: Array<T>): ReactiveArrayInstance<T> {
    const val = new Array(...elements),
        dep = new Tracker.Dependency(),
        depend = dep.depend.bind(dep),
        changed = dep.changed.bind(dep)

    function writer(accessor : any) {
        return function() {
            changed()
            // Discard return value, see comment in ReactiveArrayInstance
            accessor.apply(val, arguments)
        }
    }

    return {
        get(index?: number)
        : any   // Cannot seem to get this overloaded function to type-check in any other way
        {
            depend()
            if (typeof index === 'undefined') {
                return val
            } else {
                return val[index]
            }
        },

        getNonreactive() { return val },

        push: writer(val.push),
        splice: writer(val.splice)
    }
}
