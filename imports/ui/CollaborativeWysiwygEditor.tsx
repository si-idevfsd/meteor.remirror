import debug_ from 'debug'

import React, { ReactNodeArray, FC, useState, useEffect } from 'react'

import { SchemaSpec } from 'prosemirror-model'
import { WysiwygEditor, WysiwygEditorProps } from '@remirror/editor-wysiwyg'

import { CollaborationExtension } from "@remirror/extension-collaboration"

import { useRemirrorContext } from '@remirror/react'
import { useTrackerAutorunEffect } from '../client/lib/tracker-effects'
import { ProseMirrorDDPAPI, InitialDocumentVersion } from '../api/documents'
import { ProseMirrorClient } from '../client/documents'

const debug = debug_("tiptap:CollaborativeWysiwygEditor")

export const CollaborativeWysiwygEditor : FC<WysiwygEditorProps & {documentID: string}> = ({
    documentID,
    ...props
}) => {
    const [error, setError] = useState<Meteor.Error>()
    const [client, setClient] = useState<ProseMirrorClient>()

    useEffect(() => {
        const api = new ProseMirrorDDPAPI()
        const client = new ProseMirrorClient(api, documentID)
        client.on("error", setError)
        setClient(client)
        return () => {
            debug("Stopping API")
            client.stop()
        }
    }, [documentID])  // re-do this effect when documentID changes

    const [initialDocument, setInitialDocument] = useState<InitialDocumentVersion>()
    useTrackerAutorunEffect(() => {
        if (! client) return
        const initialDocument = client.getInitialDocument()
        if (! initialDocument) return
        setInitialDocument(initialDocument)
        debug("Initial document version %j received in %j",
              initialDocument.version, [documentID])
    }, [client])

    const retval : ReactNodeArray = []
    if (error) {
        debug("Error: " + error.toString())
        retval.push(<div key="error">{error.reason ?? error.toString()}</div>)
    }

    if (! (client && initialDocument)) {
        retval.push(<div key="loading">Loading...</div>)
        return <>{retval}</>
    }

    const extension = new CollaborationExtension(
        {
            clientID: client.prosemirrorClientID,
            version: initialDocument.version,
            onSendableReceived({jsonSendable}) { client.send(jsonSendable) }
        }),
    extensions = [ { extension, priority: 1 } ]

    retval.push(<WysiwygEditor key="editor" extensions={extensions} initialContent={initialDocument.content} {...props}>
        <Downlink client={client} serverSchemaSpec={initialDocument.schemaSpec}/>
    </WysiwygEditor>)
    return <>{retval}</>
}

const Downlink: FC<{client: ProseMirrorClient, serverSchemaSpec: SchemaSpec}> =
function Downlink({client, serverSchemaSpec}) {
    const ctx = useRemirrorContext<CollaborationExtension>()

    const [schemaChecked, setSchemaChecked] = useState(false)
    if  (! schemaChecked) {
        const clientSchemaSpec = ctx.state.newState.schema.spec
        if (JSON.stringify(clientSchemaSpec) ===
            JSON.stringify(serverSchemaSpec)) {
            setSchemaChecked(true)
        } else {
            throw new Error("Bad schema")
        }
    }

    useTrackerAutorunEffect(() => {
        const { steps, version } = client.getUpdates()
        if (! (version && steps.length)) return
        debug(`Received ${steps.length} updates from ${steps[0].clientID}`)
        if (debug.enabled) {
            console.log(steps)
        }
        ctx.actions.collaborationUpdate({steps, version})
    }, [client])
    return null  // this React component produces no DOM
}
