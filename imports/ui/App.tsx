import React, { useState } from 'react'
import { CollaborativeWysiwygEditor } from './CollaborativeWysiwygEditor'


function EditorAndButton (clazz : any) {
  return () => {
    const [editorVisible, setEditorVisible] = useState(true);

    const retval = []
    if (editorVisible) {
      retval.push(React.createElement(clazz, {documentID: "XXX_MOCK_ID", key: "editor"}))
    }
    retval.push(<button key="button" onClick={() => setEditorVisible(! editorVisible)}>{ editorVisible ? "Close" : "Open" }</button>)
    return <>{retval}</>
  }
}


export function App () {
  const // StockEditor = EditorAndButton(WysiwygEditor),
        CollaborativeEditor = EditorAndButton(CollaborativeWysiwygEditor)
  return <div><CollaborativeEditor/><CollaborativeEditor/></div>
}
