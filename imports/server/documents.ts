/**
 * Server-side state machine for the Prosemirror-over-DDP protocol
 */

import debug_ from 'debug'
import { Meteor } from 'meteor/meteor'
import { Mongo } from 'meteor/mongo'
import { Schema , Node as ProsemirrorNode } from 'prosemirror-model'
import { Step } from 'prosemirror-transform'
import { EventEmitter } from 'events'
import { ProseMirrorDDPAPI, ProseMirrorVersionID, UpdateDocumentStatus,
         InitialDocumentVersion, SubsequentDocumentVersion,
         ErrorCode, SerializableStep, SerializableNode } from '../api/documents'
import "../api/schema-spec-ejson"

// TODO: find some way to share schema-creating code between client and server.
import { wysiwygSchemaSpec } from "../../tests/fixtures/schema"
import { laCigaleJSON } from "../../tests/fixtures/doc"

const Documents = new Mongo.Collection<InitialDocumentVersion>('documents')

Meteor.startup(async () => {
    if (! (await Documents.find({}).fetchAsync()).length) {
        await Documents.insertAsync({
            _id: "XXX_MOCK_ID",
            schemaSpec: wysiwygSchemaSpec,
            content: laCigaleJSON,
            version: 42
        })
    }
})

const api = new ProseMirrorDDPAPI()

/**
 * Load the document from the database.
 *
 * TODO: this is contingent; refactor elsewhere.
 */
async function loadDocument (docID : string, _userId: string | null) :
Promise<InitialDocumentVersion> {
    const [doc] : InitialDocumentVersion[] =
        await Documents.find({_id: docID}).fetchAsync()
    if (doc) {
        return doc
    } else {
        throw new Meteor.Error(ErrorCode.DocumentNotFound,
                               `Document ${docID} not found in MongoDB`)
    }
}

function handleMoniker(connection : Meteor.Connection, collectionName: string, docID: string) {
    return `${collectionName}(${docID})@${connection.id}`
}

const debugHandle = debug_("tiptap:getDocument")

api.getDocument.publishAsync(async function (docID : string) {
      const myMoniker = handleMoniker(this.connection, this.collectionName, docID),
      debug = (blah : string) => debugHandle(`${myMoniker}: ${blah}`)

    const doc = await ConnectedDocument.open(docID, () => loadDocument(docID, this.userId))
    this.added(String(doc.version), {
        version: doc.version,
        schemaSpec: doc.schema.spec,
        content: doc.content
    })

    const stop = doc.onDocumentUpdated((connection, clientCollectionName, versions) => {
        if (connection === this.connection &&
            clientCollectionName === this.collectionName) return
        const theirMoniker = handleMoniker(connection, clientCollectionName, docID)
        debug(`Sending ${versions.length} versions to ${theirMoniker}`)
        for (const v of versions) {
            this.added(String(v.version), v)
        }
    })
    this.onStop(() => stop())
    debug("ready")
})

const debugUpdateDocument = debug_("tiptap:updateDocument")

api.updateDocument.implementAsync(async function onUpdateDocument(
    docID: string,
    clientCollectionName: string,
    version: ProseMirrorVersionID,
    steps: SerializableStep[]
) {
    const debug = (blah : string) => {
        debugUpdateDocument(`${clientCollectionName}@${docID}: ${blah}`)
    }

    const doc = await ConnectedDocument.open(docID, () => loadDocument(docID, this.userId))
    const status = doc.prepareUpdates(
        this.connection as Meteor.Connection,  // Cannot be null here
        clientCollectionName, version, steps)

    if (status instanceof UpdateReady) {
        if (status.isEmpty()) {
            debug("Received empty update")
        } else {
            debug(`${status.newVersions.length} steps accepted at version ${version}...`)
            await Documents.updateAsync(docID, { $set: {
                    version: status.lastVersion,
                    content: status.newContent
                }})
            status.commitAndBroadcast()
            debug(`..., applied to MongoDB and broadcast`)
        }
        return {
            status: UpdateDocumentStatus.OK,
            newVersion: status.lastVersion ?? version
        }
    } else if (status instanceof UpdateMustRebase) {
        debug(`${steps.length} steps received out of sequence, must rebase to ${status.mustRebaseToVersion}`)
        return {
            status: UpdateDocumentStatus.REBASE,
            newVersion: status.mustRebaseToVersion
        }
    } else if (status instanceof UpdateIllegal) {
        throw status.toError()
    } else {
        throw new Error('Unexpected type for status: ' + typeof(status))
    }
})

type OnDocumentUpdatedCallback = (connection : Meteor.Connection,
                                  clientCollectionName: string,
                                  versions: SubsequentDocumentVersion[])
                               => void

const debugConnectedDocument = debug_("tiptap::ConnectedDocument")


type PrepareUpdatesOutcome = UpdateReady | UpdateMustRebase | UpdateIllegal

/**
 * A document that is currently open in at least one client.
 */
class ConnectedDocument {
    private static byDocID : { [ key : string ] : Promise<ConnectedDocument> } = {}
    private debug(blah : string) { debugConnectedDocument(`${this.docID}: ${blah}`) }

    static async open(docID: string,
                      load: () => Promise<InitialDocumentVersion>) : Promise<ConnectedDocument> {
        if (! ConnectedDocument.byDocID[docID]) {
            ConnectedDocument.byDocID[docID] =
                load().then((initial) => {
                    const that = new ConnectedDocument(docID, initial)
                    that.debug(`opened at version ${initial.version}`)
                    return that
                })
        }
        const doc = await ConnectedDocument.byDocID[docID]
        return doc
    }

    private close () {
        this.debug("closing")
        delete ConnectedDocument.byDocID[this.docID]
    }

    public schema: Schema
    public doc : ProsemirrorNode
    public version: number
    private events = new EventEmitter()
    constructor(public docID: string, initial: InitialDocumentVersion) {
        this.schema = new Schema(initial.schemaSpec)
        this.doc = this.schema.nodeFromJSON(initial.content)
        this.version = initial.version
    }

    public get content () : SerializableNode {
        return this.doc.toJSON() as SerializableNode
    }

    public prepareUpdates(connection: Meteor.Connection,
                          clientCollectionName: string,
                          clientVersion: ProseMirrorVersionID,
                          steps: SerializableStep[]
                         ) : PrepareUpdatesOutcome
    {
        if (this.version !== clientVersion) {
            return new UpdateMustRebase({
                clientVersion,
                mustRebaseToVersion: this.version
            })
        }

        const self = this,
        success /* (... hopefully, at this point) */ = new UpdateReady({
            newVersions: [],
            newDoc: this.doc,
            commitAndBroadcast() {
                const newVersion = success.lastVersion
                if (! newVersion) return   // Null update
                self.version = newVersion
                self.doc = this.newDoc
                const updateArgs : Parameters<OnDocumentUpdatedCallback> = [
                    connection,
                    clientCollectionName,
                    success.newVersions
                ]
                self.events.emit("update", ...updateArgs)
            }
        })

        for (let i = 0; i < steps.length; i++) {
            const step = steps[i]
            const newDoc = Step.fromJSON(this.schema, step).apply(success.newDoc).doc
            if (! newDoc) {
                return new UpdateIllegal(step, i)
            }
            success.newDoc = newDoc
            success.newVersions.push({
                clientID: makeClientID(connection, clientCollectionName),
                version: this.version + i + 1,
                step
            })
        }

        return success
    }

    private observerRefcount = 0
    public onDocumentUpdated (callback: OnDocumentUpdatedCallback) {
        const events = this.events
        events.addListener("update", callback)
        this.observerRefcount += 1
        this.debug(`refcount upped to ${this.observerRefcount}`)
        return /* stop: */ () => {
            events.removeListener("update", callback)
            this.observerRefcount -= 1
            this.debug(`refcount downed to ${this.observerRefcount}`)
            if (this.observerRefcount === 0) this.close()
        }
    }
}

/**
 * @returns A string to tag updates made by this client prior to sending them to
 *          other subscribed clients
 */
// Other, more paranoid implementations are possible, including even
// <code>return "REDACTED";</code>
function makeClientID (connection : Meteor.Connection, clientCollectionName : string) {
    return `${clientCollectionName}@${connection.id}`
}

// https://github.com/Microsoft/TypeScript/issues/5326#issuecomment-592058988
interface IUpdateReady {
    newDoc: ProsemirrorNode
    newVersions: Array<SubsequentDocumentVersion>
    commitAndBroadcast: () => void
}

interface UpdateReady extends IUpdateReady {}

class UpdateReady {
    constructor(props: IUpdateReady) {
        Object.assign(this, props)
    }

    isEmpty() { return ! this.newVersions.length }

    get lastVersion () : number | undefined {
        if (! this.isEmpty()) {
            return this.newVersions[this.newVersions.length - 1].version
        }
    }

    get newContent () : SerializableNode {
        return this.newDoc.toJSON() as SerializableNode
    }
}

interface IUpdateMustRebase {
    clientVersion: number
    mustRebaseToVersion: number
}

interface UpdateMustRebase extends IUpdateMustRebase {}

class UpdateMustRebase {
    constructor(props: IUpdateMustRebase) {
        Object.assign(this, props)
    }
}

class UpdateIllegal {
    constructor(public badStep: SerializableStep, public stepIndex: number) {}
    toError() {
        return new Meteor.Error('UpdateIllegal', `Illegal update at index ${this.stepIndex}`)
    }
}
