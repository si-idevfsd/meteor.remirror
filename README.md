# Develop

## Tools for your Browser

1. Do yourself a favor and install Chrome or Chromium
1. Install following Chrome plug-ins:
    - [Meteor DevTools](https://chrome.google.com/webstore/detail/meteor-devtools/ippapidnnboiophakmmhkdlchoccbgje)
    - [React devtools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)

## First time

```
meteor npm install
meteor
```

## Day-to-day

```
TEST_BROWSER_DRIVER=chrome TEST_WATCH=1 meteor test --full-app --driver-package meteortesting:mocha
```

Tack `--inspect` or `--inspect-brk` onto that if you want to [debug the server with Chrome](https://medium.com/the-node-js-collection/debugging-node-js-with-google-chrome-4965b5f910f4)
