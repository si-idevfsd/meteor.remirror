declare module "meteor/tracker" {
    module Tracker {
        // Fix up too simple type in @types/meteor
        function nonreactive<T>(func: () => T): T;
    }
}
