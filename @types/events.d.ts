// For whatever reason, the version of this in @types/events doesn't work.

declare module "events" {
    export type Listener = (...args: any[]) => void;
}
